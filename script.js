var soma=0
var contador=0
var verifica=false

let secao=document.querySelector(".nota");
let res=document.querySelector(".resultado");


function addNumber() {
    let input=document.querySelector(".input_number");
    let number=input.value;
    
    
    if (number != "") {
        if (number >= 0 && number <= 10) {
            number=parseFloat(number);
            soma+=number;
            contador ++;
            console.log(number);

            let escopo=document.createElement("div");
            let notas=document.createElement("p");

            notas.innerText="A nota foi " + number;

            escopo.append(notas);
            secao.append(escopo);
        }
        else {

            alert("Invalido, tente novamente.")
        }
    }
    else {
        console.log(input.value);
        if (input.value=="") {
                alert("Invalido, tente novamente.")
        }
    }
    input.value="";
}


function calcMedia() {
    if (!verifica) {
        let escopo=document.createElement("div");
        let resposta=document.createElement("p");

        let media=soma/contador

        const mediaForm=media.toFixed(2);

        resposta = "A média é " + mediaForm;

        escopo.append(resposta);
        res.append(escopo);

        verifica=true;
    }
}

let adicionar=document.querySelector(".adicionar");
adicionar.addEventListener("click", () => {addNumber()});

let calc=document.querySelector(".calcular");
calc.addEventListener("click", () => {calcMedia()});
